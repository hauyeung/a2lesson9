package com.example.a2lesson9;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class ImageAdapter extends ArrayAdapter<Bitmap>{

	private Activity context;	
	private ArrayList<Bitmap> b;

	public ImageAdapter(Activity context, ArrayList<Bitmap> b)
	{
		super(context,R.layout.cameraview);
		this.context = context;		
		this.b = b;

	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.cameraview, null, true);		
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);		
		imageView.setImageBitmap(b.get(position));
		return rowView;
	}

}
